#include "Mesh.h"
#include "Utils.h"

#include <fstream>
#include <iostream>

using namespace std;

float Vertex::length() const {
    return sqrtf(x * x + y * y + z * z);
}

Vertex operator+(const Vertex &v1, const Vertex &v2) {
    return Vertex(
        v1.x + v2.x,
        v1.y + v2.y,
        v1.z + v2.z,
        v1.w + v2.w
    );
}

Vertex operator-(const Vertex &v1, const Vertex &v2) {
    return Vertex(
        v1.x - v2.x,
        v1.y - v2.y,
        v1.z - v2.z,
        v1.w - v2.w
    );
}

Vertex operator*(const Vertex &v1, const Vertex &v2) {
    return Vertex(
        v1.x * v2.x,
        v1.y * v2.y,
        v1.z * v2.z,
        v1.w * v2.w
    );
}

Vertex operator/(const Vertex &v1, const Vertex &v2) {
    return Vertex(
        v1.x / v2.x,
        v1.y / v2.y,
        v1.z / v2.z,
        v1.w / v2.w
    );
}

Vertex operator+(const Vertex &v1, float x) {
    return Vertex(
        v1.x + x,
        v1.y + x,
        v1.z + x,
        v1.w + x
    );
}

Vertex operator-(const Vertex &v1, float x) {
    return Vertex(
        v1.x - x,
        v1.y - x,
        v1.z - x,
        v1.w - x
    );
}

Vertex operator*(const Vertex &v1, float x) {
    return Vertex(
        v1.x * x,
        v1.y * x,
        v1.z * x,
        v1.w * x
    );
}

Vertex operator/(const Vertex &v1, float x) {
    return Vertex(
        v1.x / x,
        v1.y / x,
        v1.z / x,
        v1.w / x
    );
}

Mesh operator+(const Mesh &m1, const Mesh &m2) {
    assert(m1.vertices.size() == m2.vertices.size());

    auto vertices = vector<Vertex>();
    vertices.reserve(m1.vertices.size());
    
    auto it1 = m1.vertices.cbegin();
    auto it2 = m2.vertices.cbegin();
    for (; it1 != m1.vertices.cend(); it1++, it2++) {
        vertices.push_back(*it1 + *it2);
    }

    return Mesh(vertices, m1.uvs, m1.normals, m1.faces);
}

Mesh operator-(const Mesh &m1, const Mesh &m2) {
    assert(m1.vertices.size() == m2.vertices.size());

    auto vertices = vector<Vertex>();
    vertices.reserve(m1.vertices.size());
    
    auto it1 = m1.vertices.cbegin();
    auto it2 = m2.vertices.cbegin();
    for (; it1 != m1.vertices.cend(); it1++, it2++) {
        vertices.push_back(*it1 - *it2);
    }

    return Mesh(vertices, m1.uvs, m1.normals, m1.faces);
}

Mesh operator+(const Mesh &m1, float x) {
    auto vertices = vector<Vertex>();
    vertices.reserve(m1.vertices.size());
    
    auto it1 = m1.vertices.cbegin();
    for (; it1 != m1.vertices.cend(); it1++) {
        vertices.push_back(*it1 + x);
    }

    return Mesh(vertices, m1.uvs, m1.normals, m1.faces);
}

Mesh operator-(const Mesh &m1, float x) {
    auto vertices = vector<Vertex>();
    vertices.reserve(m1.vertices.size());
    
    auto it1 = m1.vertices.cbegin();
    for (; it1 != m1.vertices.cend(); it1++) {
        vertices.push_back(*it1 - x);
    }

    return Mesh(vertices, m1.uvs, m1.normals, m1.faces);
}

Mesh operator*(const Mesh &m1, float x) {
    auto vertices = vector<Vertex>();
    vertices.reserve(m1.vertices.size());
    
    auto it1 = m1.vertices.cbegin();
    for (; it1 != m1.vertices.cend(); it1++) {
        vertices.push_back(*it1 * x);
    }

    return Mesh(vertices, m1.uvs, m1.normals, m1.faces);
}

Mesh operator/(const Mesh &m1, float x) {
    auto vertices = vector<Vertex>();
    vertices.reserve(m1.vertices.size());
    
    auto it1 = m1.vertices.cbegin();
    for (; it1 != m1.vertices.cend(); it1++) {
        vertices.push_back(*it1 / x);
    }

    return Mesh(vertices, m1.uvs, m1.normals, m1.faces);
}

bool starts_with(const string &s, const string &prefix) {
    return s.rfind(prefix, 0) == 0;
}

Polygon parse_polygon(const string &line, int start, int end) {
    auto slash_index = line.find('/', start);
    if (slash_index > end) {
        auto vertex_id = stoi(line.substr(start, end - start));
        return Polygon(vertex_id);
    }

    auto vertex_id = stoi(line.substr(start, slash_index - start));

    auto next_slash_index = line.find('/', slash_index + 1);
    if (next_slash_index > end) {
        auto uv_id = stoi(line.substr(slash_index + 1, end - slash_index - 1));
        return Polygon(vertex_id, uv_id);
    }

    optional<int> uv_id;

    if (slash_index + 1 == next_slash_index) {
        uv_id = {};
    } else {
        uv_id = stoi(line.substr(slash_index + 1, end - slash_index - 1));
    }

    auto normal_id = stoi(line.substr(next_slash_index + 1, end - next_slash_index - 1));
    return Polygon(vertex_id, uv_id, normal_id);
}

optional<Mesh*> load_mesh(const std::string &file) {
    auto stream = ifstream(file);
    if (!stream.is_open()) { return {}; }
    
    vector<Vertex> vertices;
    vector<UV>     uvs;
    vector<Normal> normals;
    vector<Face>   faces;

    string line;
    while (getline(stream, line)) {
        if (line.empty() || starts_with(line, "#")) { continue; }

        if (starts_with(line, "v ")) {
            /// parse vertex
            float x, y, z, w = 1.0f;
            sscanf(line.c_str(), "v %f %f %f %f", &x, &y, &z, &w);
            vertices.push_back(Vertex(x, y, z, w));

            continue;
        }

        if (starts_with(line, "vt ")) {
            /// parse UV
            float u, v;
            sscanf(line.c_str(), "vt %f %f", &u, &v);
            uvs.push_back(UV(u, v));

            continue;
        }

        if (starts_with(line, "vn ")) {
            /// parse normal
            float x, y, z;
            sscanf(line.c_str(), "vn %f %f %f", &x, &y, &z);
            normals.push_back(Normal(x, y, z));

            continue;
        }

        if (starts_with(line, "f ")) {
            /// parse face

            vector<Polygon> polygons;
            auto start_idx = 2;
            auto end_idx   = line.find(' ', start_idx);
            while (true) {
                polygons.push_back(parse_polygon(line, start_idx, end_idx));
                start_idx = end_idx;
                end_idx = line.find(' ', end_idx + 1);
                if (end_idx == string::npos) {
                    polygons.push_back(parse_polygon(line, start_idx, line.size()));
                    break;
                }
            }

            faces.push_back(Face(polygons));

            continue;
        }

        cerr << "Unknown specifier " << line << endl;
    }

    stream.close();

    return new Mesh(vertices, uvs, normals, faces);
}

bool write_mesh(const Mesh *mesh, const std::string &file) {
    auto stream = ofstream(file);
    if (!stream.is_open()) { return false; }

    auto dump_polygon = [&stream](const Polygon &poly) {
        stream << poly.vertex_id;
        if (poly.uv_id.has_value())
            stream << "/" << poly.uv_id.value();
        if (poly.normal_id.has_value())
            stream << "/" << poly.normal_id.value();
    };

    for (auto it = mesh->vertices.cbegin(); it != mesh->vertices.cend(); it++) {
        stream << "v " << it->x << " " << it->y << " " << it->z << " " << it->w << endl;
    }

    for (auto it = mesh->uvs.cbegin(); it != mesh->uvs.cend(); it++) {
        stream << "vt " << it->u << " " << it->v << endl;
    }

    for (auto it = mesh->normals.cbegin(); it != mesh->normals.cend(); it++) {
        stream << "vn " << it->x << " " << it->y << " " << it->z << " " << endl;
    }

    for (auto it = mesh->faces.cbegin(); it != mesh->faces.cend(); it++) {
        stream << "f ";
        for (auto p = it->polygons.cbegin(); p != it->polygons.cend(); p++) {
            dump_polygon(*p);
            stream << " ";
        }
        stream << endl;
    }

    stream.close();

    return true;
}
