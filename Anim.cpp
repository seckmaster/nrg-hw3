#include "Anim.h"
#include "Mesh.h"
#include "Interp.h"

#include <fstream>
#include <iostream>
#include <unordered_map>
#include <numeric>
#include <filesystem>

using namespace std;

int Animation::animation_duration() const {
    if (keyframes.empty()) { return 0; }
    return keyframes.back().timestamp_ms;
}

optional<Animation> load_animation(const string &file) {
    auto stream = ifstream(file);
    if (!stream.is_open()) { return {}; }

    vector<Keyframe> keyframes;

    string line;
    while (getline(stream, line)) {
        auto space = line.find(' ');
        if (space == string::npos) { return {}; }
        
        auto obj_file  = line.substr(0, space);
        auto timestamp =  stoi(line.substr(space + 1, line.size() - space - 1));

        keyframes.push_back(Keyframe(obj_file, timestamp));
    }
    stream.close();
    return Animation(keyframes);
}

AnimationContext* load_and_prepare_animation(const std::string &anim_file_path, const std::string &model_search_path) {
    auto anim = load_animation(anim_file_path);
    if (!anim.has_value()) { return nullptr; }

    auto context = new AnimationContext(anim.value());

    auto map = unordered_map<string, weak_ptr<Mesh>>();
    for (auto it = context->animation.keyframes.begin(); it != context->animation.keyframes.end(); it++) {
        if (map.contains(it->file)) {
            it->model = map[it->file];
        } else {
            auto model = load_mesh(model_search_path + it->file);
            if (!model.has_value()) {
                delete context;
                return nullptr;
            }

            context->models.push_back(shared_ptr<Mesh>(model.value()));
            map[it->file] = weak_ptr<Mesh>(context->models.back());
            it->model     = weak_ptr<Mesh>(context->models.back());
        }
    }

    return context;
}

vector<vector<tuple<float, float>>> length_reparametrization(const Interpolator<Mesh*> &fn_dt, float dt) {
    auto table = vector<vector<tuple<float, float>>>();
    table.reserve(static_cast<size_t>(1.0f / dt) + 1);

    for (float t = 0.0f; t <= 1.0f; t += dt) {
        auto d  = fn_dt(t);
        auto dv = *d * (t == 0.0f ? 0.0f : dt);

        auto row = vector<tuple<float, float>>();
        row.reserve(dv.vertices.size());

        int idx = 0;
        for (auto it = dv.vertices.cbegin(); it != dv.vertices.cend(); it++, idx++) {
            auto prev_value = table.empty() ? 0 : get<1>(table.back()[idx]);
            row.push_back(make_tuple(t, it->length() + prev_value));
        }
        table.push_back(row);

        delete d;
    }
    
    return table;
}

void perform_animation(const AnimationContext &context, const AnimationConfig &cfg, const std::string &output_path) {
    filesystem::remove_all(output_path);
    filesystem::create_directory(output_path);

    int ctr = 1;

    {
        /// First spline ...
        /// @TODO: - 
        auto k0 = (context.animation.keyframes.cbegin() + 0)->model.lock().get();
        auto k1 = (context.animation.keyframes.cbegin() + 1)->model.lock().get();
    }

    for (auto it = context.animation.keyframes.cbegin() + 1; it != context.animation.keyframes.cend() - 2; it++) {
        auto k1 = (it - 1)->model.lock().get();
        auto k2 = (it + 0)->model.lock().get();
        auto k3 = (it + 1)->model.lock().get();
        auto k4 = (it + 2)->model.lock().get();

        auto duration = (it + 1)->timestamp_ms - it->timestamp_ms;
        auto frames   = cfg.fps * duration / 1000;

        if (cfg.is_speed_control_enabled) {
            /// @FIXME: Not the fastest nor the most elegant code ... 

            auto fn_dt = cfg.method == BEZIER 
                ? bezier::interp_dt(*k1, *k2, *k3, *k4) 
                : catmull_rom::uniform::interp_dt(*k1, *k2, *k3, *k4, cfg.tau);
        
            auto placeholder = vector<Vertex>();
            placeholder.reserve(k1->vertices.size());
            auto meshes = vector<Mesh>(frames, Mesh(placeholder, k1->uvs, k1->normals, k1->faces));

            auto table = length_reparametrization(fn_dt, cfg.length_reparametrization_step);

            for (int i = 0; i < k1->vertices.size(); i++) { // for each vertex
                auto fn = cfg.method == BEZIER 
                    ? bezier::interp(k1->vertices[i], k2->vertices[i], k3->vertices[i], k4->vertices[i])
                    : catmull_rom::uniform::interp(k1->vertices[i], k2->vertices[i], k3->vertices[i], k4->vertices[i], cfg.tau);

                auto ds = get<1>(table.back()[i]) / static_cast<float>(frames);
                auto prev_idx = 1;

                auto mesh = 0;
                auto s    = 0.0f;
                for (; s <= get<1>(table.back()[i]); s += ds) {  // s := arc length
                    if (mesh == 0) {
                        assert(mesh < meshes.size());
                        meshes[mesh++].vertices.push_back(fn(0.0f));
                        continue;
                    }
                    if (mesh >= frames) break; // @TODO: This check should not be needed, provide fix in future !!

                    auto el = table[prev_idx][i];
                    if (s > get<1>(el)) {
                        do ++prev_idx;
                        while (s > get<1>(table[prev_idx][i]));
                        s -= ds;
                        continue;
                    }
                    auto prev_el = table[prev_idx - 1][i];
                    
                    auto t = (s - get<1>(prev_el)) / (get<1>(el) - get<1>(prev_el));
                    auto u = lerp(get<0>(prev_el),
                                  get<0>(el), 
                                  t);
                    
                    assert(mesh < meshes.size());
                    meshes[mesh++].vertices.push_back(fn(u));
                }
                // if (s < get<1>(table.back()[i])) {
                //     meshes[frames - 1].vertices.push_back(fn(1.0f));
                // }
            }

            cout << meshes.size() << endl;
            for (auto mesh = meshes.cbegin(); mesh != meshes.cend(); mesh++) {
                write_mesh(&*mesh, output_path + "anim_" + to_string(ctr) + ".obj");
                ctr++;
            }

            continue;
        }

        if (!cfg.is_speed_control_enabled) {
            auto fn = cfg.method == BEZIER ? bezier::interp(*k1, *k2, *k3, *k4) : catmull_rom::uniform::interp(*k1, *k2, *k3, *k4, cfg.tau);;
            auto du = 1.0f / static_cast<float>(frames);

            for (float u = 0.0f; u <= 1.0f; u += du) {
                auto mesh = fn(u);
                write_mesh(mesh, output_path + "anim_" + to_string(ctr) + ".obj");
                delete mesh;
                ctr++;
            }
        }
    }

    {
        /// Last spline ...
        /// @TODO: - 
        auto k0 = (context.animation.keyframes.cend() - 2)->model.lock().get();
        auto k1 = (context.animation.keyframes.cend() - 1)->model.lock().get();
    }
}
