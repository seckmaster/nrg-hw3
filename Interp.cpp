#include "Interp.h"
#include "Utils.h"

#include <algorithm>
#include <iostream>
#include <tuple>

using namespace std;

/// @TODO: There is some code duplication going on in this file,
/// let's think about how we can do better ...

namespace catmull_rom {
    namespace uniform {
        tuple<float, float, float, float>coefficients(float k1, float k2, float k3, float k4, float tau) {
            auto a = -tau * k1 + (2.0f - tau) * k2 + (tau - 2.0f) * k3 + tau * k4;
            auto b = 2.0f * tau * k1 + (tau - 3.0f) * k2 + (3.0f - 2.0f * tau) * k3 - tau * k4;
            auto c = -tau * k1 + tau * k3;
            auto d = k2;
            return make_tuple(a, b, c, d);
        }

        ///

        Interpolator<float> interp(float k1, float k2, float k3, float k4, float tau) {
            auto tuple = coefficients(k1, k2, k3, k4, tau);
            return [tuple](float u) {
                return powf(u, 3) * get<0>(tuple) + powf(u, 2) * get<1>(tuple) + u * get<2>(tuple) + get<3>(tuple);
            };
        }

        Interpolator<float> interp_dt(float k1, float k2, float k3, float k4, float tau) {
            auto tuple = coefficients(k1, k2, k3, k4, tau);
            return [tuple](float u) {
                return 3 * powf(u, 2) * get<0>(tuple) + 2 * u * get<1>(tuple) + get<2>(tuple);
            };
        }

        Interpolator<Vertex> interp(const Vertex& k1, const Vertex& k2, const Vertex& k3, const Vertex& k4, float tau) {
            auto f1 = interp(k1.x, k2.x, k3.x, k4.x, tau);
            auto f2 = interp(k1.y, k2.y, k3.y, k4.y, tau);
            auto f3 = interp(k1.z, k2.z, k3.z, k4.z, tau);
            auto f4 = interp(k1.w, k2.w, k3.w, k4.w, tau);
            return [f1, f2, f3, f4](float u) {
                return Vertex(
                    f1(u),
                    f2(u),
                    f3(u),
                    f4(u)
                );
            };
        }

        Interpolator<Vertex> interp_dt(const Vertex& k1, const Vertex& k2, const Vertex& k3, const Vertex& k4, float tau) {
            auto f1 = interp_dt(k1.x, k2.x, k3.x, k4.x, tau);
            auto f2 = interp_dt(k1.y, k2.y, k3.y, k4.y, tau);
            auto f3 = interp_dt(k1.z, k2.z, k3.z, k4.z, tau);
            auto f4 = interp_dt(k1.w, k2.w, k3.w, k4.w, tau);
            return [f1, f2, f3, f4](float u) {
                return Vertex(
                    f1(u),
                    f2(u),
                    f3(u),
                    f4(u)
                );
            };
        }

        Interpolator<Mesh*> interp(const Mesh& k1, const Mesh& k2, const Mesh& k3, const Mesh& k4, float tau) {
            /// @NOTE: It is assumed that all meshes have the same number of 'vertices' and other stuff ...
            
            auto functions = vector<Interpolator<Vertex>>();
            functions.reserve(k1.vertices.size());

            auto it1 = k1.vertices.cbegin();
            auto it2 = k2.vertices.cbegin();
            auto it3 = k3.vertices.cbegin();
            auto it4 = k4.vertices.cbegin();

            for (; it1 != k1.vertices.cend(); it1++, it2++, it3++, it4++) {
                auto fn = interp(*it1, *it2, *it3, *it4, tau);
                functions.push_back(fn);
            }
            
            return [functions, &k1](float u) {
                auto vertices = fmap<Interpolator<Vertex>, Vertex>(functions, [u](auto &f) { return f(u); });
                auto mesh = new Mesh(vertices, k1.uvs, k1.normals, k1.faces);
                return mesh;
            };
        }

        Interpolator<Mesh*> interp_dt(const Mesh& k1, const Mesh& k2, const Mesh& k3, const Mesh& k4, float tau) {
            /// @NOTE: It is assumed that all meshes have the same number of 'vertices' and other stuff ...

            auto functions = vector<Interpolator<Vertex>>();
            functions.reserve(k1.vertices.size());

            auto it1 = k1.vertices.cbegin();
            auto it2 = k2.vertices.cbegin();
            auto it3 = k3.vertices.cbegin();
            auto it4 = k4.vertices.cbegin();

            for (; it1 != k1.vertices.cend(); it1++, it2++, it3++, it4++) {
                auto fn = interp_dt(*it1, *it2, *it3, *it4, tau);
                functions.push_back(fn);
            }
            
            return [functions, &k1](float u) {
                auto vertices = fmap<Interpolator<Vertex>, Vertex>(functions, [u](auto &f) { return f(u); });
                auto mesh = new Mesh(vertices, k1.uvs, k1.normals, k1.faces);
                return mesh;
            };
        }
    } // namespace uniform
} // namespace catmull_rom

namespace bezier {
    tuple<float, float, float, float>coefficients(float k1, float k2, float k3, float k4) {
        auto a = -k1 + 3 * k2 - 3 * k3 + k4;
        auto b = 3 * k1 -6 * k2 + 3 * k3;
        auto c = -3 * k1 + 3 * k2;
        auto d = k4;
        return make_tuple(a, b, c, d);
    }

    Interpolator<float> interp(float k1, float k2, float k3, float k4) {
        auto tuple = coefficients(k1, k2, k3, k4);
        return [tuple](float u) {
            return powf(u, 3) * get<0>(tuple) + powf(u, 2) * get<1>(tuple) + u * get<2>(tuple) + get<3>(tuple);
        };
    }

    Interpolator<float> interp_dt(float k1, float k2, float k3, float k4) {
        auto tuple = coefficients(k1, k2, k3, k4);
        return [tuple](float u) {
            return 3 * powf(u, 2) * get<0>(tuple) + 2 * u * get<1>(tuple) + get<2>(tuple);
        };
    }

    Interpolator<Vertex> interp(const Vertex& k1, const Vertex& k2, const Vertex& k3, const Vertex& k4) {
        auto f1 = interp(k1.x, k2.x, k3.x, k4.x);
        auto f2 = interp(k1.y, k2.y, k3.y, k4.y);
        auto f3 = interp(k1.z, k2.z, k3.z, k4.z);
        auto f4 = interp(k1.w, k2.w, k3.w, k4.w);
        return [f1, f2, f3, f4](float u) {
            return Vertex(
                f1(u),
                f2(u),
                f3(u),
                f4(u)
            );
        };
    }

    Interpolator<Vertex> interp_dt(const Vertex& k1, const Vertex& k2, const Vertex& k3, const Vertex& k4) {
        auto f1 = interp_dt(k1.x, k2.x, k3.x, k4.x);
        auto f2 = interp_dt(k1.y, k2.y, k3.y, k4.y);
        auto f3 = interp_dt(k1.z, k2.z, k3.z, k4.z);
        auto f4 = interp_dt(k1.w, k2.w, k3.w, k4.w);
        return [f1, f2, f3, f4](float u) {
            return Vertex(
                f1(u),
                f2(u),
                f3(u),
                f4(u)
            );
        };
    }
    
    Interpolator<Mesh*> interp(const Mesh& k1, const Mesh& k2, const Mesh& k3, const Mesh& k4) {
        /// @NOTE: It is assumed that all meshes have the same number of 'vertices' and other stuff ...
        
        auto functions = vector<Interpolator<Vertex>>();
        functions.reserve(k1.vertices.size());

        auto it1 = k1.vertices.cbegin();
        auto it2 = k2.vertices.cbegin();
        auto it3 = k3.vertices.cbegin();
        auto it4 = k4.vertices.cbegin();

        for (; it1 != k1.vertices.cend(); it1++, it2++, it3++, it4++) {
            auto fn = interp(*it1, *it2, *it3, *it4);
            functions.push_back(fn);
        }
        
        return [functions, &k1](float u) {
            auto vertices = fmap<Interpolator<Vertex>, Vertex>(functions, [u](auto &f) { return f(u); });
            auto mesh = new Mesh(vertices, k1.uvs, k1.normals, k1.faces);
            return mesh;
        };
    }

    Interpolator<Mesh*> interp_dt(const Mesh& k1, const Mesh& k2, const Mesh& k3, const Mesh& k4) {
        /// @NOTE: It is assumed that all meshes have the same number of 'vertices' and other stuff ...

        auto functions = vector<Interpolator<Vertex>>();
        functions.reserve(k1.vertices.size());

        auto it1 = k1.vertices.cbegin();
        auto it2 = k2.vertices.cbegin();
        auto it3 = k3.vertices.cbegin();
        auto it4 = k4.vertices.cbegin();

        for (; it1 != k1.vertices.cend(); it1++, it2++, it3++, it4++) {
            auto fn = interp_dt(*it1, *it2, *it3, *it4);
            functions.push_back(fn);
        }
        
        return [functions, &k1](float u) {
            auto vertices = fmap<Interpolator<Vertex>, Vertex>(functions, [u](auto &f) { return f(u); });
            auto mesh = new Mesh(vertices, k1.uvs, k1.normals, k1.faces);
            return mesh;
        };
    }
}