#ifndef _UTILS_H_
#define _UTILS_H_

#include <vector>
#include <algorithm>

using namespace std;

template<class T, class U>
vector<U> fmap(const vector<T> &vec, function<U(const T&)> map_to) {
    auto mapped = vector<U>();
    transform(vec.begin(), vec.end(), 
            back_inserter(mapped), 
            map_to);
    return mapped;
}

#endif