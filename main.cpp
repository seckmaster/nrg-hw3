#include <iostream>

#include "Anim.h"
#include "arg_parser.h"

using namespace std;

int main(int argc, char **argv) {
    auto output_dir    = string("anim_03/");
    auto animation     = string("../data/input_03.txt");
    auto search_path   = string("../data/");
    auto speed_control = true;
    auto tau           = 0.5f;
    auto fps           = 60;
    auto method        = CATMULL_ROM;

    auto args = to_string(argv, argc);

    {
        auto dir = parse_arg(args, "output_path");
        if (dir.has_value()) {
            output_dir = dir.value();
        }
    }

    {
        auto animm = parse_arg(args, "animation");
        if (animm.has_value()) {
            animation = animm.value();
        }
    }

    {
        auto path = parse_arg(args, "search_path");
        if (path.has_value()) {
            search_path = path.value();
        }
    }

    {
        auto control = parse_bool(args, "speed_control");
        if (control.has_value()) {
            speed_control = control.value();
        }
    }

    {
        auto tau_ = parse_double(args, "tau");
        if (tau_.has_value()) {
            tau = (float) tau_.value();
        }
    }

    {
        auto fps_ = parse_int(args, "fps");
        if (fps_.has_value()) {
            fps = fps_.value();
        }
    }

    {
        auto method_ = parse_arg(args, "method");
        if (method_.has_value()) {
            if (method_.value() == "catmull_rom") {
                method = CATMULL_ROM;
            } else if (method_.value() == "bezier") {
                method = BEZIER;
            }
        }
    }

    auto ctx = load_and_prepare_animation(animation, search_path);
    if (ctx == nullptr) {
        cerr << "Animation failed to load ..." << endl;
        return 1;
    }

    auto cfg = AnimationConfig(tau, fps, speed_control, 0.5f, method);
    perform_animation(*ctx, cfg, output_dir);

    delete ctx;

    return 0;
}
