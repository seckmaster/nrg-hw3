#ifndef _INTERP_H_
#define _INTERP_H_

#include <functional>
#include "Mesh.h"

template <typename T>
using Interpolator = std::function<T(float)>;

namespace catmull_rom {
    namespace uniform {
        Interpolator<float>  interp(float k1, float k2, float k3, float k4, float tau);
        Interpolator<float>  interp_dt(float k1, float k2, float k3, float k4, float tau);

        Interpolator<Vertex> interp(const Vertex& k1, const Vertex& k2, const Vertex& k3, const Vertex& k4, float tau);
        Interpolator<Vertex> interp_dt(const Vertex& k1, const Vertex& k2, const Vertex& k3, const Vertex& k4, float tau);
        
        Interpolator<Mesh*>  interp(const Mesh& k1, const Mesh& k2, const Mesh& k3, const Mesh& k4, float tau);
        Interpolator<Mesh*>  interp_dt(const Mesh& k1, const Mesh& k2, const Mesh& k3, const Mesh& k4, float tau);
    }

    /// @TODO: Implement chordal and centripental parametrizations.
}

namespace bezier {
    Interpolator<float>  interp(float k1, float k2, float k3, float k4);
    Interpolator<float>  interp_dt(float k1, float k2, float k3, float k4);

    Interpolator<Vertex> interp(const Vertex& k1, const Vertex& k2, const Vertex& k3, const Vertex& k4);
    Interpolator<Vertex> interp_dt(const Vertex& k1, const Vertex& k2, const Vertex& k3, const Vertex& k4);

    Interpolator<Mesh*>  interp(const Mesh& k1, const Mesh& k2, const Mesh& k3, const Mesh& k4);
    Interpolator<Mesh*>  interp_dt(const Mesh& k1, const Mesh& k2, const Mesh& k3, const Mesh& k4);
}

#endif
