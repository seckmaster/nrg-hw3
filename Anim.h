#ifndef _ANIM_H_
#define _ANIM_H_

#include <optional>
#include <vector>
#include <string>
#include <memory>

struct Mesh;

struct Keyframe {
    /// .obj file name
    std::string file;

    /// keyframe timestamp
    int timestamp_ms;

    /// ptr to mesh
    /// we have it in the Keyframe for fast look-up
    std::weak_ptr<Mesh> model;

    Keyframe(const std::string &file, int timestamp_ms)
        : file(file), timestamp_ms(timestamp_ms) {}
};

struct Animation {
    std::vector<Keyframe> keyframes;

    Animation(const std::vector<Keyframe> &keyframes) : keyframes(keyframes) {}

    int animation_duration() const;
};

std::optional<Animation> load_animation(const std::string &file);

struct AnimationContext { 
    std::vector<std::shared_ptr<Mesh>> models;
    Animation animation;

    AnimationContext(const Animation &animation) : animation(animation) {}
};

AnimationContext* load_and_prepare_animation(const std::string &anim_file_path, const std::string &model_search_path);

enum InterpolationMethod {
    CATMULL_ROM,
    BEZIER
};

struct AnimationConfig {
    float               tau;
    int                 fps;
    bool                is_speed_control_enabled;
    float               length_reparametrization_step;
    InterpolationMethod method;

    AnimationConfig(float tau, int fps, bool is_speed_control_enabled,
                    float length_reparametrization_step,
                    InterpolationMethod method)
        : tau(tau),
          fps(fps),
          is_speed_control_enabled(is_speed_control_enabled),
          length_reparametrization_step(length_reparametrization_step),
          method(method) {}
};

void perform_animation(const AnimationContext &context, const AnimationConfig &cfg, const std::string &output_path);

#endif
