#ifndef _OBJ_PARSER_H
#define _OBJ_PARSER_H

#include <vector>
#include <optional>

struct Vertex {
    float x;
    float y;
    float z;
    float w;

    Vertex() : x(0.0f), y(0.0f), z(0.0f), w(1.0f) {}

    Vertex(float x, float y, float z, float w) : x(x), y(y), z(z), w(1.0f) {}

    float length() const;
};

Vertex operator-(const Vertex &v1, const Vertex &v2);
Vertex operator+(const Vertex &v1, const Vertex &v2);
Vertex operator*(const Vertex &v1, const Vertex &v2);
Vertex operator/(const Vertex &v1, const Vertex &v2);

Vertex operator+(const Vertex &v1, float x);
Vertex operator-(const Vertex &v1, float x);
Vertex operator*(const Vertex &v1, float x);
Vertex operator/(const Vertex &v1, float x);

struct UV {
    float u;
    float v;

    UV() : u(0.0f), v(0.0f) {}
    
    UV(float u, float v) : u(u), v(v) {}
};

struct Normal {
    float x;
    float y;
    float z;

    Normal() : x(0.0f), y(0.0f), z(0.0f) {}

    Normal(float x, float y, float z) : x(x), y(y), z(z) {}
};

struct Polygon {
    typedef int ID;

    ID vertex_id;
    std::optional<ID> uv_id;
    std::optional<ID> normal_id;

    Polygon(ID vertex_id, std::optional<ID> uv_id = {},
         std::optional<ID> normal_id = {})
        : vertex_id(vertex_id), uv_id(uv_id), normal_id(normal_id) {}
};

struct Face {
    std::vector<Polygon> polygons;

    Face(std::vector<Polygon> polygons) : polygons(polygons) {}
};

struct Mesh {
    std::vector<Vertex> vertices;
    std::vector<UV>     uvs;
    std::vector<Normal> normals;
    std::vector<Face>   faces;

    Mesh(std::vector<Vertex> vertices, std::vector<UV> uvs,
          std::vector<Normal> normals, std::vector<Face> faces)
        : vertices(vertices), uvs(uvs), normals(normals), faces(faces) {}
};

/// @NOTE: The following operators operate only on vertices.
/// Faces, normals, and UVs are copied from one of the parameters.
Mesh operator+(const Mesh &m1, const Mesh &m2);
Mesh operator-(const Mesh &m1, const Mesh &m2);
Mesh operator*(const Mesh &m1, const Mesh &m2);
Mesh operator/(const Mesh &m1, const Mesh &m2);

Mesh operator+(const Mesh &m1, float x);
Mesh operator-(const Mesh &m1, float x);
Mesh operator*(const Mesh &m1, float x);
Mesh operator/(const Mesh &m1, float x);

std::optional<Mesh*> load_mesh(const std::string &file);
bool write_mesh(const Mesh *mesh, const std::string &file);

#endif
